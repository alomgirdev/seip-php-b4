<?php

class Category{
    public $conn;
    public function __construct()
    {
        session_start();
        try{
            $dsn = "mysql:host=localhost;dbname=ecommerceone;charset=UTF8";
            $this->conn = new PDO( $dsn,"root","root");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exception)
        {
            // echo "Connection Fail";
            //echo $exception->getMessage();
            $_SESSION['message'] = $exception->getMessage();
            header("location: Create.php");
        }
    }    

    public function store($POST)
    {
        try{
            $title = $POST['title'];
            $description = $POST['description'];
            $query = "insert into category(title,discription) values(:category_title,:category_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_title'=>$title,
                'category_description'=>$description,
            ]);
            $_SESSION['message'] = 'Uploaded Successfully';
            header("location: Create.php");
        }catch(PDOException $exception)
        {
            //echo $exception->getMessage(); 
            $_SESSION['message'] = $exception->getMessage();
            header("location: Create.php");
        }

    }
}








?>