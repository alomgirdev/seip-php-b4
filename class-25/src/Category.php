<?php

class Category{
    public $conn;
    public function __construct()
    {
        
        try{
            session_start();
            $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce","root","1234");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exception)
        {
            // echo "Connection Fail";
            //echo $exception->getMessage();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../views/Category/Create.php");
        }
    }   
    
    public function index()
    {
        
        $query = "select * from category order by id desc";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
        
        return $data;
    }

    public function store($POST)
    {
        try{
            $title = $POST['title'];
            $description = $POST['description'];
            $query = "insert into category(title,description) values(:category_title,:category_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_title'=>$title,
                'category_description'=>$description,
            ]);
            $_SESSION['message'] = 'Uploaded Successfully';
            header("location: ../Category/index.php");
        }catch(PDOException $exception)
        {
            //echo $exception->getMessage(); 
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Category/Create.php");
        }

    }
}








?>