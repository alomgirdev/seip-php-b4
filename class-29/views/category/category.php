<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css" rel="stylesheet">
    <title>Hello, world!</title>


    <style>
      .my-btn{
    display: inline-block;
    font-weight: 400;
    text-align: center;
    white-space: nowrap;
    vertical-align: middle;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
    border: 1px solid transparent;
    border-top-color: transparent;
    border-right-color: transparent;
    border-bottom-color: transparent;
    border-left-color: transparent;
    padding: .375rem .75rem;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    margin: 10px;
    transition: color .15s ease-in-out,background-color .15s ease-in-out,border-color .15s ease-in-out,box-shadow .15s ease-in-out;
}
    </style>
  </head>
  <body>
    
    <?php
        //include_once('../../src/Product.php');
        include_once('../../src/Category.php');

        // $productObj = new Product();
        // $products = $productObj->showProductList($productCategory);

        $categoryObj = new Category();
        $categorys = $categoryObj->getCategoryList();

        $productCategory = $_GET['category'] ?? null;
       


        // echo $productCategory;
        // echo '<pre>';
        // print_r($products);
        
    ?>




  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Navbar</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="../index.php">Home</a>
        </li>
        <li class="nav-item">
         
        </li>
        
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
            Dropdown
          </a>
          <ul class="dropdown-menu" aria-labelledby="navbarDropdown">
              <?php foreach($categorys as $category){ ?>

                    <li><a class="dropdown-item" href="index.php?category=<?=$category['id']?>"><?=$category['category_name']?></a></li>

                <?php } ?>
          </ul>
        </li>
      </ul>
      <form class="d-flex">
       
      </form>
    </div>
  </div>
</nav>

<div class="m-3 mx-5">
<a class="nav-link btn btn-primary d-inline text-white" href="addCategory.php">Add new Category</a>

<div class="row mt-3 ">



<table class="table">
  <thead class="thead-dark bg-dark text-white">
    <tr>
      <th scope="col">#</th>
      <th scope="col">Name</th>
      <th scope="col">Description</th>
      <th scope="col text-center">Action</th>
    </tr>
  </thead>
  <tbody>
  <?php $i=1; foreach($categorys as $category){?>
    <tr>
      <th scope="row"><?=$i++;?></th>
      <td><?=$category['category_name']?></td>
      <td><?=$category['diescription']?></td>
      <td>
      <div class="d-flex justify-content-center">
            <a href="edit.php?id=<?=$category['id']?>" class="my-btn btn-success text-white"><i class="fas fa-edit"></i></a> 
            <a href="delete.php?id=<?=$category['id']?>" class="my-btn btn-danger text-white"><i class="fa fa-trash" aria-hidden="true"></i></a>
        </div>
      </td>
    </tr>
    <?php }?>
  </tbody>
</table>
</div>
</div>









    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>