<?php
    class Category{
        
        public $con;
        public function __construct(){
            

            try {
                $this->con = new PDO("mysql:host=localhost;dbname=crud", "root", "root");
            } catch (PDOException $e) {
                echo $_SESSION['message'] = $e->getMessage();
            }
        }
        public function addCategory($data){
           
            session_start();

            $name = $data['name'];
            $diescription = $data['diescription'];

            try{
                $query = "insert into category(category_name, diescription) values(:category_name, :diescription)";
                $statement = $this->con->prepare($query);
                $statement->execute([
                    ':category_name' => $name,
                    ':diescription' => $diescription,
                    
                ]);
                header("location: ../category/addCategory.php");
            }catch(PDOException $e){
                $_SESSION['massege'] = $e->getMessage();
                header("location: ../category/addCategory.php");
            }
        }

        public function getCategoryList(){
            try{

                $query = "select *from category where is_deleted=0"; //distinct
                $statement = $this->con->prepare($query);
                $statement->execute();
                $data = $statement->fetchAll();
                //print_r($data);
                return $data;
            }catch(PDOException $e){
                $_SESSION['massege'] = $e->getMessage();
                //header("location: ../views/index.php");
            }
        }
        public function getCategory($id){
            try{
                $query = "select *from category where id=$id"; //distinct
                $statement = $this->con->prepare($query);
                $statement->execute();
                $data = $statement->fetch();
                //print_r($data);
                return $data;
            }catch(PDOException $e){
                $_SESSION['massege'] = $e->getMessage();
                //header("location: ../views/index.php");
            }
        }
        public function update($data){
            session_start();

            $name = $data['name'];
            $diescription = $data['diescription'];
            $id = $data['id'];

            try{
                $query = "UPDATE category SET category_name=:category_name, diescription=:diescription WHERE id=:id";
                $statement = $this->con->prepare($query);
                $statement->execute([
                    ':category_name' => $name,
                    ':diescription' => $diescription,
                    ':id' => $id,
                ]);
                header("location: ../category/addCategory.php");
            }catch(PDOException $e){
                $_SESSION['massege'] = $e->getMessage();
                header("location: ../category/edit.php");
            }
        }
        public function distroy($id){
            try{
                $query = "UPDATE category SET is_deleted=:is_deleted WHERE id=:id";
                $statement = $this->con->prepare($query);
                $statement->execute([
                    'is_deleted' => 1,
                    'id' => $id,
                ]);
                
                header("location: ..category/category.php");
            }catch(PDOException $e){
                echo $_SESSION['massege'] = $e->getMessage();
            }
        }

        

        
    }


?>