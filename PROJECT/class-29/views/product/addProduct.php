<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

    <title>Hello, world!</title>
  </head>
  <body>
    
    <?php
        include_once('../../src/Product.php');
        include_once('../../src/Category.php');
        $categoryObj = new Category();

        $categorys = $categoryObj->getCategoryList();


    ?>




  <nav class="navbar navbar-expand-lg navbar-light bg-light">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">Nebula</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarSupportedContent">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item">
          <a class="nav-link active" aria-current="page" href="../admin/admin.php">Dashbord</a>
        </li>
        
        
      </ul>
     
    </div>
  </div>
</nav>




<div class="container">
<form action="add.php" method="POST">
  <div class="mb-3">

    <label for="exampleInputEmail1" class="form-label">Category</label>
                
    <select class="form-select" name="category" aria-label="Default select example">

      <?php foreach($categorys as $category){ ?>

          <option value="<?=$category['id']?>"> <?=$category['category_name']?></option>
          
      <?php } ?>

    </select>


    
  </div>
  <div class="mb-3">
    <label for="exampleInputPassword1" class="form-label">Title</label>
    <input type="text" name="title" class="form-control" id="exampleInputPassword1">
  </div>
  <div class="mb-3 ">
  <label for="exampleInputPassword1" class="form-label">Diescription</label>
    <Textarea name="diescription" class="form-control" id="exampleInputPassword1"></textarea>
  </div>
  <div class="mb-3 ">
  <label for="price" class="form-label">Price</label>
    <input type="number" name="price" class="form-control" id="price">
  </div>
  <div class="mb-3 ">
  <label for="discount" class="form-label">Discount</label>
    <input type="number" name="discount" class="form-control" id="discount">
  </div>
  <div class="mb-3 ">
  <label for="image" class="form-label">Image</label>
    <input type="file" name="image" class="form-control" id="image">
  </div>
  <button type="submit" class="btn btn-primary">Add</button>
</form>
</div>






    <!-- Optional JavaScript; choose one of the two! -->

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>

    <!-- Option 2: Separate Popper and Bootstrap JS -->
    <!--
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    -->
  </body>
</html>