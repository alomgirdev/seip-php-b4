<?php
    class Product{
        public $con;
        public function __construct(){
            session_start();

            try {
                $this->con = new PDO("mysql:host=localhost;dbname=crud", "root", "root");
            } catch (PDOException $e) {
                $_SESSION['message'] = $e->getMessage();
            }
        }
        public function addProduct($data){
            

            $category = $data['category'];
            $title = $data['title'];
            $diescription = $data['diescription'];
            $price = $data['price'];
            $discount = $data['discount'];

            try{
                $query = "insert into products(category_id, title, diescription, price, discount, in_stok) values(:category, :title, :diescription, :price, :discount, :in_stok)";
                $statement = $this->con->prepare($query);
                $statement->execute([
                    ':category' => $category,
                    ':title' => $title,
                    ':diescription' => $diescription,
                    ':price' => $price,
                    ':discount' => $discount,
                    ':in_stok' => 1,
                    
                ]);
                header("location: ../product/addProduct.php");
            }catch(PDOException $e){
                $_SESSION['massege'] = $e->getMessage();
                header("location: ../views/addProduct.php");
            }
        }

        public function getProduct($id){
            try{
                $query = "select *from products where id=$id"; 
                $statement = $this->con->prepare($query);
                $statement->execute();
                return $data = $statement->fetch();
            }catch(PDOException $e){

            }
        }

        public function showProductList($categoryId = null)
        {
            // echo $category;
            try{

                if($categoryId){
                    $query = "select *from products where category_id='$categoryId' and is_deleted=0"; 
                }
                else{
                    $query = "select *from products where is_deleted=0"; 
                }
                $statement = $this->con->prepare($query);
                $statement->execute();
                $data = $statement->fetchAll();
                //print_r($data);
                return $data;
            }catch(PDOException $e){
                $_SESSION['massege'] = $e->getMessage();
                //header("location: ../views/index.php");
            }
        }

        public function showProductDetails($productId){
            try{

                $query = "select *from products where id='$productId'";
                $statement = $this->con->prepare($query);
                $statement->execute();
                $data = $statement->fetch();

                return $data;

            }catch(PDOException $e){

            }
        }

        public function update($data){
            $category = $data['category'];
            $title = $data['title'];
            $diescription = $data['diescription'];
            $price = $data['price'];
            $discount = $data['discount'];
            $in_stok = isset($data['in_stok'])?1:0;
            $id = $data['id'];

            try{
                $query = "UPDATE products SET 
                category_id=:category, 
                title=:title,
                diescription=:diescription,
                price=:price,
                discount=:discount,
                in_stok=:in_stok
                WHERE id=:id";
                $statement = $this->con->prepare($query);
                $statement->execute([
                    ':category' => $category,
                    ':title' => $title,
                    ':diescription' => $diescription,
                    ':price' => $price,
                    ':discount' => $discount,
                    ':in_stok' => $in_stok,
                    ':id' => $id
                    
                ]);
                header("location: ../admin/admin.php");
            }catch(PDOException $e){
                $_SESSION['massege'] = $e->getMessage();
                header("location: ../views/addProduct.php");
            }
        }

        public function getTrastList(){
            try{

              
                $query = "select *from products where is_deleted=1"; 
               
                $statement = $this->con->prepare($query);
                $statement->execute();
                $data = $statement->fetchAll();
                //print_r($data);
                return $data;
            }catch(PDOException $e){
                $_SESSION['massege'] = $e->getMessage();
                header("location: ../product/trast.php");
            }
        }

        public function restor($id){
            try{
                $query = "UPDATE products SET is_deleted=:is_deleted WHERE id=:id";
                $statement = $this->con->prepare($query);
                $statement->execute([
                    'is_deleted' => 0,
                    'id' => $id,
                ]);
                //$_SESSION['massege'] = $e->getMessage();
                header("location: ../product/trast.php");
            }catch(PDOException $e){
                
            }
        }
        

        public function distroy($id){
            try{
                $query = "UPDATE products SET is_deleted=:is_deleted WHERE id=:id";
                $statement = $this->con->prepare($query);
                $statement->execute([
                    'is_deleted' => 1,
                    'id' => $id,
                ]);
                //$_SESSION['massege'] = $e->getMessage();
                header("location: ../index.php");
            }catch(PDOException $e){
                
            }
        }

        
    }


?>