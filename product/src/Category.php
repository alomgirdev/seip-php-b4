<?php

class Category{
    public $conn;
    public function __construct()
    {
        
        try{
            session_start();
            $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce","root","1234");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exception)
        {
            // echo "Connection Fail";
            //echo $exception->getMessage();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../views/Category/Create.php");
        }
    }   
    
    public function index()
    {
        
        $query = "select * from categories order by id desc";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
        
        return $data;
    }

    public function store($POST)
    {
        // print_r($POST);
        // die();
        try{
            $title = $POST['title'];
            $quantity = $POST['quantity'];
            $price = $POST['price'];
            $description = $POST['description'];

            $query = "insert into categories(title,quantity, price, description) values(:category_title, :category_quantity, :category_price,:category_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_title'=>$title,
                'category_quantity'=>$quantity,
                'category_price'=>$price,
                'category_description'=>$description
            ]);
            $_SESSION['message'] = 'Added Successfully';
            header("location: ../Category/index.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Category/Create.php");
            //echo "ok";
        }

    }
}
