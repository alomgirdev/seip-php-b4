<?php

class Selary{
    public $conn;
    public function __construct()
    {
        
        try{
            session_start();
            $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce","root","1234");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exception)
        {
            // echo "Connection Fail";
            //echo $exception->getMessage();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../views/Selary/Create.php");
        }
    }   
    
    public function index()
    {
        
        $query = "select * from selary order by id desc";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
        
        return $data;
    }

    public function store($POST)
    {
        // print_r($POST);
        // die();
        try{
            $name = $POST['name'];
            $title = $POST['title'];
            $amount = $POST['amount'];
            $absent = $POST['absent'];
            $meg = $POST['meg'];

            $query = "insert into selary(name,title,amount, absent, meg) values(:selary_name,:selary_title, :selary_amount, :selary_absent,:selary_meg)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'selary_name'=>$name,
                'selary_title'=>$title,
                'selary_amount'=>$amount,
                'selary_absent'=>$absent,
                'selary_meg'=>$meg
            ]);
            $_SESSION['message'] = 'Added Successfully';
            header("location: ../Selary/index.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Selary/Create.php");
            //echo "ok";
        }

    }

    public function show($id)
    {
        
        $query = "select * from selary where id = :selary_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'selary_id'=> $id
            ]);
            $data = $stmt->fetch();
            // echo '<pre>';
            // print_r($data);
            return $data;
    }

    public function edit($id)
    {
        //echo $id;
        
        $query = "select * from selary where id = :selary_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'selary_id'=> $id
            ]);
            $data = $stmt->fetch();
            //  echo '<pre>';
            //  print_r($data);
            return $data;
    }

    public function update($POST)
    {
        // echo '<pre>';
        // print_r($POST);
        // echo '</pre>';
        // die();
        try{
            $id =$POST['id'];
            $name = $POST['name'];
            $title = $POST['title'];
            $amount = $POST['amount'];
            $absent = $POST['absent'];
            $message = $POST['meg'];

            $query = "update selary set name=:selary_name,title=:selary_title,amount=:selary_amount,
             absent=:selary_absent, message=:selary_meg where id=:selary_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'selary_name'=>$name,
                'selary_title'=>$title,
                'selary_amount'=>$amount,
                'selary_absent'=>$absent,
                'selary_meg'=>$message,
                'selary_id'=>$id
            ]);
            $_SESSION['message'] = 'Updated Successfully';
            header("location: ../Selary/index.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Selary/Edit.php?id=$id");
            //echo "ok";
        }

    }

    public function destroy($data)
    {
        $id = $data['id'];
        try{
            $query = "delete from selary where id=:selary_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    ':selary_id'=> $id,
                ]);
            $_SESSION['message'] = 'Successfully Deleted';   
            header("location: ../Selary/index.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Selary/index.php");
            //echo "ok";
        }
    }
}