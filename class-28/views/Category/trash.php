<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Category List</title>
</head>
<body>

<?php
// include_once('../../src/User.php');

include_once'../../vendor/autoload.php';
use Alom\User;

$categoryObject = new User();
$getdata = $categoryObject->trash();
// echo '<pre>';
//  print_r($getdata);
//  echo '</pre>';
?>
    <?php
        if(isset($_SESSION['message'])){
            echo $_SESSION['message']."<br>";
            unset($_SESSION['message']);
        }
    ?>
    <a href="index.php">List</a>
    <table border="1">
        <thead>
            <tr>
                <th>SL</th>
                <th>Name</th>
                
                <th>Picture</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        
            <?php 
                $sl = 1;
                foreach($getdata as $data)
                { ?>

                 <tr>
                    <td><?= $sl++ ?></td>
                    <td><?php echo $data['name'] ?></td>
                    
                    <td><img src="../../assets/images/<?=$data['picture']?>" style="height: 100px; width: 100px;" alt=""></td>
                    <td>
                        | <a href="restore.php ?id=<?= $data['id']?>">Restore </a> 
                         |<a href="permanet_delete.php ?id=<?= $data['id']?>" onclick=" return confirm('Are You Sure want to permanent delete?')">Permanent Delete </a> </td>
                </tr>
                
            <?php }
            ?>
            
        </tbody>
        
    </table>
</body>
</html>