<?php
namespace Alom;
use PDO;
use PDOException;
class Course{
    public $conn;
    public function __construct()
    {
        
        try{
            // session_start();
            $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce","root","1234");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exception)
        {
            // echo "Connection Fail";
            //echo $exception->getMessage();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../views/Category/Registration.php");
        }
    }   
    
    public function courseByCategoryId($id = null)
    {
        // die('Course Method');

        if($id){
            $query = "select * from courses where category_id='$id' order by id desc";
        }else{
            $query = "select * from courses  order by id desc"; 
        }
        
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
        
        return $data;
    }
    public function show($id)
    {
        
        $query = "select * from courses where id= :category_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_id'=> $id
            ]);
            $data = $stmt->fetch();
            // echo '<pre>';
            // print_r($data);
           return $data;
    }

}