<?php
namespace Alom;
use PDO;
use PDOException;
class User{
    public $conn;
    public function __construct()
    {
        
        try{
            session_start();
            $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce","root","1234");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exception)
        {
            // echo "Connection Fail";
            //echo $exception->getMessage();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../views/Category/Registration.php");
        }
    }   
    
    public function index()
    {
        
        $query = "select * from birthregister where is_deleted is null order by id desc";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
        
        return $data;
    }

    public function store($POST,$FILE)
    {
        // print_r($POST);
        // die();
        try{
            // echo '<pre>'; 
            $fileName = $FILE['picture']['name'];
            $tempName = $FILE['picture']['tmp_name'];
            $explodeArray = explode('.',$fileName);
            $fileExtension = end ($explodeArray);
            $aceptableImageType = ['jpg','jpeg','gif','png','PNG'];

            if(!in_array($fileExtension, $aceptableImageType)){
                $_SESSION['message'] = 'Invalid Image Type';
                 header("location: ../Category/Registration.php");
            }

            $uniqueImageName = time().$fileName;
            move_uploaded_file($tempName,'../../assets/images/'.$uniqueImageName);

            // die();
            $name = $POST['name'];
            $father = $POST['father'];
            $mother = $POST['mother'];
            $date = $POST['date'];
            $gende = $POST['gende'];
            $mobile = $POST['mobile'];
            $mail = $POST['mail'];
            $description = $POST['descriptio'];

            $query = "insert into birthregister(name,father, mother, date, gende, mobile, mail,descriptio, picture) values(:name, :father, :mother,:date,:gende,:mobile, :mail, :descriptio, :picture)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                ':name'=>$name,
                ':father'=>$father,
                ':mother'=>$mother,
                ':date'=>$date,
                ':gende' =>$gende,
                ':mobile' =>$mobile,
                ':mail' =>$mail,
                ':descriptio' =>$description,
                ':picture' =>$uniqueImageName,
                
               
            ]);
            $_SESSION['message'] = 'Added Successfully';
            header("location: ../Category/index.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Category/Registration.php");
            //echo "ok";
        }

    }

    public function show($id)
    {
        
        $query = "select * from birthregister where id = :category_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_id'=> $id
            ]);
            $data = $stmt->fetch();
            // echo '<pre>';
            // print_r($data);
            return $data;
    }

    public function edit($id)
    {
        //echo $id;
        
        $query = "select * from birthregister where id = :category_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_id'=> $id
            ]);
            $data = $stmt->fetch();
            //  echo '<pre>';
            //  print_r($data);
            return $data;
    }

    public function update($POST,$FILE)
    {
        echo '<pre>';
        print_r($POST);
        echo '</pre>';

        echo '<pre>';
        print_r($FILE);
        echo '</pre>';
        // die();
       
           
           

             try{
                // echo '<pre>'; 
                $fileName = $FILE['picture']['name'];
                $tempName = $FILE['picture']['tmp_name'];
                $explodeArray = explode('.',$fileName);
                $fileExtension = end ($explodeArray);
                $aceptableImageType = ['jpg','jpeg','gif','png','PNG'];
    
                if(!in_array($fileExtension, $aceptableImageType)){
                    $_SESSION['message'] = 'Invalid Image Type';
                     header("location: ../Category/Registration.php");
                }
    
                echo $uniqueImageName = time().$fileName;
                move_uploaded_file($tempName,'../../assets/images/'.$uniqueImageName);
    
                // die();
                $id = $POST['id'];
        
                $name = $POST['name'];
             
                $father = $POST['father'];
             
                $mother = $POST['mother'];
               
                $date = $POST['date'];
               
                $gende = $POST['gende'];
              
                $mobile = $POST['mobile'];
              
                $mail = $POST['mail'];
               
                
    
                
                // $query = "insert into birthregister(name,father, mother, date, date, mobile, mail, picture) values(:name, :father, :mother,:date,:gende,:mobile, :mail, :picture)";
                
                $query = "update birthregister set 
                name=:name,
                father=:father,
                mother=:mother,
                date=:date,
                gende=:gende,
                mobile=:mobile,
                mail=:mail,
                picture=:picture  
                where id=:id"; //,father=:father,mother=:mother, date=:date, gende=:gende, mobile:mobile, mail:mail, picture:picture
                
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    ':name'=>$name,
                    ':father'=>$father,
                    ':mother'=>$mother,
                    ':date'=>$date,
                    ':gende' =>$gende,
                    ':mobile' =>$mobile,
                    ':mail' =>$mail,
                    ':picture' =>$uniqueImageName,
                    ':id' => $id, 
                ]);
                $_SESSION['message'] = 'Added Successfully';
                header("location: ../Category/index.php");
            }catch(PDOException $exception)
            {
                // echo $exception->getMessage();
                // die();
                $_SESSION['message'] = $exception->getMessage();
                header("location: ../Category/edit.php?id=$id");
                // echo "ok";
            }

    }

    public function destroy($data)
    {
        $id = $data['id'];
        try{
            // $query = "delete from birthregister where id=:category_id";
            $query = "update birthregister set is_deleted=:deleted where id=:category_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    'deleted'=>1,
                    ':category_id'=> $id,
                ]);
            $_SESSION['message'] = 'Successfully Deleted';   
            header("location: ../Category/index.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Category/index.php");
            //echo "ok";
        }
    }
    public function delete($data)
    {
        $id = $data['id'];
        try{
            $query = "delete from birthregister where id=:category_id";
            // $query = "update birthregister set is_deleted=:deleted where id=:category_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    // 'deleted'=>1,
                    ':category_id'=> $id,
                ]);
            $_SESSION['message'] = 'Successfully Deleted';   
            header("location: ../Category/trash.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Category/trash.php");
            //echo "ok";
        }
    }
    public function trash()
    {
        $query = "select * from birthregister where is_deleted=1 order by id desc";
        $stmt = $this->conn->prepare($query);
        $stmt->execute();
        $data = $stmt->fetchall();
    
    return $data;  
    }
    public function restore($data)
    {
        $id = $data['id'];
        try{
            $query = "delete from birthregister where id=:category_id";
            // $query = "update birthregister set is_deleted=:deleted where id=:category_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    // 'deleted'=>1,
                    ':category_id'=> $id,
                ]);
            $_SESSION['message'] = 'Successfully Deleted';   
            header("location: ../Category/trash.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Category/trash.php");
            //echo "ok";
        }
    }
}