<x-frontend.layouts.master>

<div class="row">

@foreach ($products as $product)
   <div class="col-md-3">
   <div class="card-deck mb-3 text-center">
            <div class="card mb-4 box-shadow">
                <div class="card-header">
                    <img src="{{ asset('storage/products/'.$product->image)}}" alt="">
                </div>
                <div class="card-body">
                    <h4 class="card-title pricing-card-title"> <a href=""> {{ Str::limit($product->title, 20) }}</a></h4>
                    <p>{{ $product->price}} TK</p>
                    <button type="button" class="btn btn-lg btn-block btn-outline-primary">Add  To Card</button>
                </div>
            </div>

            
    </div>
   </div>
@endforeach
</div>
{{ $products->links() }}
</x-frontend.layouts.master>