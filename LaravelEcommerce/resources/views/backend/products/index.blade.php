<x-backend.layouts.master>

    <x-slot:title>
        Dashboard
        </x-slot>
        <!-- Page Heading -->
        <h1 class="h3 mb-2 text-gray-800">Products</h1>
        <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
            For more information about DataTables, please visit the <a target="_blank" href="https://datatables.net">official DataTables documentation</a>.</p>
        <a class="btn btn-sm btn-primary" href="{{ route('products.create')}} ">Add New</a>
        <!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
                <h6 class="m-0 font-weight-bold text-primary text-center">Products List</h6>
            </div>

            @if(Session::has('message'))
            <div class="alert-box success">
                <h2 class="text-center">{{ Session::get('message') }}</h2>
            </div>
            @endif

            <div class="card-body">

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>

                @endif


                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Price</th>
                                <th>Office</th>
                                <th>Action</th>

                            </tr>
                        </thead>

                        <tbody>

                            @foreach($products as $product)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $product->title }}</td>
                                <td>{{ $product->price }} Tk</td>
                                <td>

                                    <a class="btn btn-primary" href="{{ route('products.show',['id'=>$product->id])}}">Show</a>
                                    |
                                    <a class="btn btn-dark" href="{{ route('products.edit',['id'=>$product->id])}}">Edit</a>
                                    |
                                    <form action="{{ route('products.destroy',['id'=>$product->id])}}" method="POST" style="display:inline">
                                        @csrf
                                        @method('delete')
                                        <button type="submit" class="btn btn-danger" onclick="return confirm('Are You Sure want to delete?')" >Delete</button>
                                        
                                    </form>
                                </td>
                            </tr>

                            @endforeach


                        </tbody>
                    </table>
                </div>
            </div>
        </div>
</x-backend.layouts.master>