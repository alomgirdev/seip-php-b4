<x-backend.layouts.master>

<x-slot:title>
        Dashboard
</x-slot>
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Products</h1>
                    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
                        For more information about DataTables, please visit the <a target="_blank"
                            href="https://datatables.net">official DataTables documentation</a>.</p>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Edit Product</h6>
                        </div>
                        <a class="btn btn-sm btn-primary" href="{{ route('products.index') }}">Product List</a>
                        <div class="card-body">
                            

                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        <form action="{{ route('products.update',['id' => $product->id ]) }}" method="POST">
                            @csrf
                            @method('patch')
                            <div class="mb-3">
                                <label for="title" class="form-label">Title</label>
                                <input name="title" type="text" class="form-control" id="title" value="{{ old('title', $product->title) }}">
                                
                                @error('title')
                                <div  class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>
                            <div class="mb-3">
                                <label for="price" class="form-label">Price</label>
                                <input name="price" type="number" class="form-control" id="price" value="{{ old('price', $product->price) }}">

                                @error('price')
                                <div  class="text-danger">{{ $message }}</div>
                                @enderror
                            </div>

                            <button type="submit" class="btn btn-primary">Update</button>
                            </form>
                        </div>
                    </div>
</x-backend.layouts.master>