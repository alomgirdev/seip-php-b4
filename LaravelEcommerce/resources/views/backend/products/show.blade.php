<x-backend.layouts.master>

<x-slot:title>
        Dashboard
</x-slot>
  <!-- Page Heading -->
  <h1 class="h3 mb-2 text-gray-800">Products</h1>
                    <p class="mb-4">DataTables is a third party plugin that is used to generate the demo table below.
                        For more information about DataTables, please visit the <a target="_blank"
                            href="https://datatables.net">official DataTables documentation</a>.</p>
                            <h6 class="m-0 font-weight-bold text-primary text-center">Products Details</h6>
                            <a class="btn btn-primary" href="{{ route('products.index')}} ">Product List</a>
                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-body">

                       <h3>Title: {{ $product->title }}</h3>
                       <p>Description: {{ $product->description }}</p>
                       <p>Price: {{ $product->price }}</p>
                       <img src="{{ asset('storage/products/'. $product->image) }}" alt="">
                       <p>Created At: {{ $product->created_at->diffForHumans() }}</p>
                       <p>Updated At: {{ $product->updated_at->diffForHumans() }}</p>
                        </div>
                    </div>
</x-backend.layouts.master>