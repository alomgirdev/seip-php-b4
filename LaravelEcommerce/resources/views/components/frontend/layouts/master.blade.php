<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <x-frontend.layouts.partials.css /> 
</head>
<body>
<x-frontend.layouts.partials.nav /> 

<div class="container">
    {{ $slot }}
</div>



 <x-frontend.layouts.partials.script />   
</body>
</html>