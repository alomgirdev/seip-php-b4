<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Http\Request;

class FrontendController extends Controller
{
    public function welcome()
    {

        $products = Product::orderBy('id', 'desc')->paginate(8);
      
        return view('welcome',compact('products'));
    }
}
