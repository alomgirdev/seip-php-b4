<?php

namespace App\Http\Controllers;

use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::orderBy('id','desc')->get();
        return view('backend.products.index', compact('products'));
    }
    public function create()
    {
        return view('backend.products.create');
    }

    public function store(Request $request)
    {
        try{

            $request->validate([
                "title" => "required|min:5|unique:products,title",
                "price" => "required|numeric",
             ]);


            // $postData = request()->all();
            

             Product::create([
            // 'title1' => $postData['title'],
            // 'price' => $postData['price']
            'title' => $request->title,
            'price' => $request->price
        ]);
        // Session::flush('message', 'Added Successfully');
        // return redirect()->route('products.index')->with('message', 'Added Successfully');

        return redirect()->route('products.index')->withMessage('Added Successfully');
        }
         catch( QueryException $e){
             return redirect()->back()->withInput()->withErrors($e->getMessage());

    }
    }

    public function show($id)
    {
        $product = Product::findOrFail($id);
        // $productName = 'T-Shirt';
        return view('backend.products.show', compact('product'));
    }
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        // $productName = 'T-Shirt';
        return view('backend.products.edit', compact('product'));
    }
    public function update(Request $request, $id)
    {
        try{
            $product = Product::findOrFail($id);

        $product->update([
            'title' => $request->title,
            'price' => $request->price
        ]);
        return redirect()->route('products.index')->withMessage('Updated Successfully');
        }catch( QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }
    public function destroy($id)
    {
        $product = Product::findOrFail($id)->delete();
        return redirect()->route('products.index')->withMessage('Successfully Deleted');
    }

}

