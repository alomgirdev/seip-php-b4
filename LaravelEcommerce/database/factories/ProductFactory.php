<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\User>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        $path = 'public/storage/products';

        return [
            'title' => $this->faker->sentence(),
            'price' => $this->faker->numberBetween(500, 1000),
            'description' => $this->faker->realText(500),
            'image' => $this->faker->image($path, 250, 300),
            // 'image' => $this->faker->image('public/storage/products',250,300, null, false),
        ];
    }


}
