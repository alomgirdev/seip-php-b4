
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Category Create</title>
</head>
<body>    

    <?php
        include_once('../../src/User.php');
        
        $userObject = new User();
        $data = $userObject->edit($_GET['id']);
        echo '<pre>';
        // print_r( $data);
    ?>

    <?php
        // session_start();
        if(isset($_SESSION['message'])){
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        
    ?> 
    <form action="Update.php" method="POST"  enctype="multipart/form-data">
        <input type="text" id="id" name="id" placeholder="Write your full name" value="<?=$data['id']?>" hidden><br>
        <label for="name">Name :</label>
        <input type="text" id="name" name="name" placeholder="Write your full name" value="<?=$data['name']?>"><br>
        <label for="father">Father's Name :</label>
        <input type="text" id="father" name="father" placeholder="Write Your Father's Name" value="<?=$data['father']?>"><br>
        <label for="mother">Mother's Name :</label>
        <input type="text" id="mother" name="mother" placeholder="Write Your Mother's Name" value="<?=$data['mother']?>"><br>
        <label for="date">Date Of Birth :</label><br>
        <input type="date" id="date" name="date"  value="<?=$data['date']?>"><br>

        <legend>Choose your gender:</legend>

        <label for="male">Male</label>
        <input type="radio" name="gende" id="male" value="male"  <?=$data['gende'] =='male'?'checked':''?>>
        
        <label for="female">Female</label>
        <input type="radio" name="gende" id="female" value="female" <?=$data['gende'] =='female'?'checked':''?>><br>
        
        <label for="mobile">Mobile :</label>
        <input type="number" id="mobile" name="mobile" placeholder="Mobile" value="<?=$data['mobile']?>"><br>
        <label for="mail">E-Mail :</label>
        <input type="text" id="mail" name="mail" placeholder="E-mail" value="<?=$data['mail']?>"><br>
        <label for="des">Description</label><br>
        <img src="../../assets/images/<?=$data['picture']?>" style="height: 100px; width: 100px;" alt="">
        <input type="file" name="picture"><br>
        <button type="submit">Update</button>  <a href="index.php" style="padding: 5px; color:white; background: green; text-decoration: none; border-radius: 5px;" >Cancel</a>
        
    </form>
</body>
</html>