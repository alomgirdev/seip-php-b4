<?php

class Category{
    public $conn;
    public function __construct()
    {
        
        try{
            session_start();
            $this->conn = new PDO("mysql:host=localhost;dbname=ecommerce","root"," ");
            $this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }catch(PDOException $exception)
        {
            // echo "Connection Fail";
            //echo $exception->getMessage();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../views/Category/Create.php");
        }
    }   
    
    public function index()
    {
        
        $query = "select * from categories order by id desc";
            $stmt = $this->conn->prepare($query);
            $stmt->execute();
            $data = $stmt->fetchall();
        
        return $data;
    }

    public function store($POST)
    {
        // prin t_r($POST);
        // die();
        try{
            $title = $POST['title'];
            $quantity = $POST['quantity'];
            $price = $POST['price'];
            $description = $POST['description'];

            $query = "insert into categories(title,quantity, price, description) values(:category_title, :category_quantity, :category_price,:category_description)";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_title'=>$title,
                'category_quantity'=>$quantity,
                'category_price'=>$price,
                'category_description'=>$description
            ]);
            $_SESSION['message'] = 'Added Successfully';
            header("location: ../Category/index.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Category/Create.php");
            //echo "ok";
        }

    }

    public function show($id)
    {
        
        $query = "select * from categories where id = :category_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_id'=> $id
            ]);
            $data = $stmt->fetch();
            // echo '<pre>';
            // print_r($data);
            return $data;
    }

    public function edit($id)
    {
        //echo $id;
        
        $query = "select * from categories where id = :category_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_id'=> $id
            ]);
            $data = $stmt->fetch();
            //  echo '<pre>';
            //  print_r($data);
            return $data;
    }

    public function update($POST)
    {
        // echo '<pre>';
        // print_r($POST);
        // echo '</pre>';
        // die();
        try{
            $id =$POST['id'];
            $title = $POST['title'];
            $quantity = $POST['quantity'];
            $price = $POST['price'];
            $description = $POST['description'];

            $query = "update categories set title=:category_title,quantity=:category_quantity,
             price=:category_price, description=:category_description where id=:category_id";
            $stmt = $this->conn->prepare($query);
            $stmt->execute([
                'category_title'=>$title,
                'category_quantity'=>$quantity,
                'category_price'=>$price,
                'category_description'=>$description,
                'category_id'=>$id
            ]);
            $_SESSION['message'] = 'Updated Successfully';
            header("location: ../Category/index.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Category/Edit.php?id=$id");
            //echo "ok";
        }

    }

    public function destroy($data)
    {
        $id = $data['id'];
        try{
            $query = "delete from categories where id=:category_id";
                $stmt = $this->conn->prepare($query);
                $stmt->execute([
                    ':category_id'=> $id,
                ]);
            $_SESSION['message'] = 'Successfully Deleted';   
            header("location: ../Category/index.php");
        }catch(PDOException $exception)
        {
            // echo $exception->getMessage();
            // die();
            $_SESSION['message'] = $exception->getMessage();
            header("location: ../Category/index.php");
            //echo "ok";
        }
    }
}