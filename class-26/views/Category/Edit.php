
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Category Create</title>
</head>
<body>    

    <?php
        include_once('../../src/Category.php');
        
        $categoryObject = new Category();
        $categorytitle = $categoryObject->edit($_GET['id']);
       // print_r( $categorytitle);
    ?>

    <?php
        // session_start();
        if(isset($_SESSION['message'])){
            echo $_SESSION['message'];
            unset($_SESSION['message']);
        }
        
    ?> 
    <form action="Update.php" method="POST">
        <input type="text"  name="id" value="<?= $categorytitle['id']?>"><br>
        <label for="title">Title</label><br>
        <input type="text" id="title" name="title" value="<?= $categorytitle['title']?>" placeholder="Enter Title Here"><br>
        <label for="number">Quantity</label><br>
        <input type="number" id="number" name="quantity" value="<?= $categorytitle['quantity']?>" placeholder="Enter Quantity"><br>
        <label for="nm1">Price</label><br>
        <input type="number" id="nm1" name="price" value="<?= $categorytitle['price']?>" placeholder="Enter Price"><br>
        <label for="des">Description</label><br>
        <textarea name="description" id="des" cols="30" rows="10"><?= $categorytitle['description']?></textarea><br>
        <button type="submit">Update</button>
    </form>
</body>
</html>