<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Category List</title>
</head>
<body>

<?php
include_once('../../src/Category.php');

$categoryObject = new Category();
$getdata = $categoryObject->index();
// echo '<pre>';
//  print_r($getdata);
//  echo '</pre>';
?>
    <?php
        if(isset($_SESSION['message'])){
            echo $_SESSION['message']."<br>";
            unset($_SESSION['message']);
        }
    ?>
    <a href="Create.php">Add New</a>
    <table border="1">
        <thead>
            <tr>
                <th>SL</th>
                <th>Title</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Description</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
        
            <?php 
                $sl = 1;
                foreach($getdata as $data)
                { ?>

                 <tr>
                    <td><?= $sl++ ?></td>
                    <td><?php echo $data['title'] ?></td>
                    <td><?php echo $data['quantity'] ?></td>
                    <td><?php echo $data['description'] ?></td>
                    <td>
                       <a href="Show.php ?id=<?= $data['id']?>">Show </a> 
                        | <a href="Edit.php ?id=<?= $data['id']?>">Edit </a> 
                         |<a href="Delete.php ?id=<?= $data['id']?>">Delete </a> </td>
                </tr>
                
            <?php }
            ?>
            
        </tbody>
        
    </table>
</body>
</html>